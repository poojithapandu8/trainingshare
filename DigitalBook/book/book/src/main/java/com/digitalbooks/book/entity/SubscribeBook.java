package com.digitalbooks.book.entity;
import java.time.LocalDate;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SubscribeBook {
	@Id
	private String subscriptionId;
	private Integer userId;
	private Date subscriptionDate;
	private Integer bookId;
	private String bookSubscribe;	
}
