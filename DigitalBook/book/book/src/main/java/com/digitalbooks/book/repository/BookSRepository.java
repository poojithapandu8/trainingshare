package com.digitalbooks.book.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.digitalbooks.book.entity.Book;
import com.digitalbooks.book.entity.SubscribeBook;

public interface BookSRepository extends JpaRepository<SubscribeBook, Integer> {

	@Modifying
    @Query(value = "UPDATE subscribe_book SET book_subscribe=?4, subscription_date=?3 WHERE book_id=?2 and user_id=?1",nativeQuery = true)
    Integer bookUSubscribe(Integer aId,Integer bookId, LocalDate localDate,String status);
	
	@Modifying
    @Query(value = "UPDATE subscribe_book SET book_subscribe=?4, subscription_date=?3 WHERE book_id=?2 and user_id=?1",nativeQuery = true)
    Integer bookSubscribeU(Integer aId,Integer bookId, LocalDate localDate,String status);
		
	  @Query(value = "select * from subscribe_book where book_id=?2 and user_id=?1 and book_subscribe='yes'",nativeQuery = true)
	  SubscribeBook bookFetchCAll(Integer Id,Integer aId);
	  
	  @Query(value = "select count(*) from subscribe_book where book_id=?1 and user_id=?2",nativeQuery = true)
	    Integer bookFetchC(Integer Id,Integer aId);
	  
	  @Query(value = "select count(*) from subscribe_book where book_id=?1 and subscription_id=?2 and book_subscribe='yes'",nativeQuery = true)
		Integer bookFetchCheck(Integer authorId, String subscriptionId);   
	     
		@Modifying
		 @Query(value = "insert into subscribe_book(book_subscribe,subscription_id,subscription_date, book_id,user_id) values (?4,?2,?3,?1,?5)",nativeQuery = true)
		void bookSubscribe(Integer bookId, String formatted, LocalDate localDate,String status,Integer aId);
		
		
		 @Query(value = "select * from subscribe_book where user_id=?1",nativeQuery = true)
			List<SubscribeBook> bookFetch(Integer aId);
}
