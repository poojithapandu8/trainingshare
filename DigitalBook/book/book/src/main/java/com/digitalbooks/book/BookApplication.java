package com.digitalbooks.book;

import com.digitalbooks.book.entity.Book;
import com.digitalbooks.book.entity.BookCategory;
import com.digitalbooks.book.repository.BookRepository;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.LocalDate;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableSwagger2
public class BookApplication implements CommandLineRunner {
    private static final Logger log = LoggerFactory.getLogger(BookApplication.class);
    @Autowired
    BookRepository bookRepository;

    public BookApplication() {
    }

    public static void main(String[] args) {
        SpringApplication.run(BookApplication.class, args);
    }
    
    @Bean
    public Docket productApi() {
       return new Docket(DocumentationType.SWAGGER_2).select()
          .apis(RequestHandlerSelectors.basePackage("com.digitalbooks.book")).build();
    }

    public void run(String... args) throws Exception {
        /*Book book = new Book(1, "The Lost", 1, BookCategory.ADVENTURE, "Publisher", 500.0D, "Content", true ,LocalDate.of(2022, 2, 10), "qwew");
        Book book1 = new Book(2, "one grilfriend", 2, BookCategory.SUSPENSE, "ram studios", 3000.0D, "the story is good", true, LocalDate.of(1998, 2, 10), "String bookImage");
        Book book2 = new Book(3, "the dawn", 1, BookCategory.DRAMA, "sita studios", 5000.0D, "the story is good 1", false, LocalDate.of(2000, 2, 10), "String bookImage1");
        Book book3 = new Book(4, "The Lost 2", 1, BookCategory.ADVENTURE, "Publisher", 500.0D, "Content", true ,LocalDate.of(2022, 2, 10), "qwew");
        Book book4 = new Book(5, "one grilfriend 2", 2, BookCategory.SUSPENSE, "ram studios", 3000.0D, "the story is good", true, LocalDate.of(1998, 2, 10), "String bookImage");
        Book book5 = new Book(6, "the dawn 2", 2, BookCategory.DRAMA, "sita studios", 5000.0D, "the story is good 1", false, LocalDate.of(2000, 2, 10), "String bookImage1");
       
        this.bookRepository.save(book); this.bookRepository.save(book5);
        this.bookRepository.save(book1); this.bookRepository.save(book4);
        this.bookRepository.save(book2); this.bookRepository.save(book3);*/
        log.info("###Created user table and inserted 3 rows!!!###");
    }
}
