package com.digitalbooks.book.service;

import com.digitalbooks.book.entity.Book;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;

public interface BookService {
    ResponseEntity<?> searchBook(String category, String title, String author, Double price, String publisher);

   // ResponseEntity<?> bookFetchAllSubscribe(Integer bookAuthorId);

    ResponseEntity<?> bookCreation(Book book, Integer authorId);

    ResponseEntity<?> bookBlocking(Integer bookId, Integer AuthorId);

    ResponseEntity<?> bookUnblocking(Integer bookId, Integer AuthorId);

    ResponseEntity<?> bookSubscribing(Integer bookId,Integer aId);

    ResponseEntity<?> bookUpdate(Book book, Integer authorId, Integer bookId);

	ResponseEntity<?> bookFetchSubscribe(Integer authorId, String subscriptionId);

	ResponseEntity<?> cancelSubscriptionBook(Integer emailId, String subscriptionId);

	List<Book> bookFetchAllSubscribe(Integer authorId);
}
