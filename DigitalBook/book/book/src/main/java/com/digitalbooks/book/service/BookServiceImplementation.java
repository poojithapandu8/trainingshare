package com.digitalbooks.book.service;

import com.digitalbooks.book.entity.Book;
import com.digitalbooks.book.entity.SubscribeBook;
import com.digitalbooks.book.entity.UserResponse;
import com.digitalbooks.book.repository.BookRepository;
import com.digitalbooks.book.repository.BookSRepository;

import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.util.Date;
import java.util.*;
import java.util.Random;

import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

@Service
@Transactional
@Slf4j
public class BookServiceImplementation implements BookService {
    private static final Logger log = LoggerFactory.getLogger(BookServiceImplementation.class);
    @Autowired
    BookRepository bookrepository;
    
    @Autowired
    BookSRepository booksrepository;

    public BookServiceImplementation() {
    }

    public ResponseEntity<?> bookBlocking(Integer bookId, Integer AuthorId) {
    	 log.info("###BookServiceImplementation - Blocking####");
    	 log.info("Book Starus:"+ this.bookrepository.bookStatusUpdate(bookId, AuthorId, false)+"bookid:"+bookId+"AuthorId"+AuthorId);
    	 this.bookrepository.bookStatusUpdate(bookId, AuthorId, false);
        UserResponse userResponse=new UserResponse();
        log.info("Book Status : "+this.bookrepository.bookStatus(bookId, AuthorId));
        if(this.bookrepository.bookStatus(bookId, AuthorId).equals("false")) {
        	userResponse.setMessage("Updated Sucessfully!!");
        	userResponse.setCode("ok");
        		return new ResponseEntity<>(userResponse, HttpStatus.OK); }
        else {
        	userResponse.setMessage("Failed to update!!");
        	userResponse.setCode("ok");
        			return new ResponseEntity<>(userResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
         
    }

    public ResponseEntity<?> bookUnblocking(Integer bookId, Integer AuthorId) {
        Integer i = this.bookrepository.bookStatusUpdate(bookId, AuthorId, true);
        UserResponse userResponse=new UserResponse();
        log.info("###BookServiceImplementation - Unblocking####" + i + " test : " + this.bookrepository.bookStatus(bookId, AuthorId));
        if(this.bookrepository.bookStatus(bookId, AuthorId).equals("true") ) { 
        		userResponse.setMessage("Updated Sucessfully!!");
    	userResponse.setCode("ok");
    		return new ResponseEntity<>(userResponse, HttpStatus.OK); }
    else {
    	userResponse.setMessage("Failed to update!!");
    	userResponse.setCode("ok");
    			return new ResponseEntity<>(userResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    }

    public ResponseEntity<?> bookCreation(Book book, Integer authorId) {
    	log.info("###BookServiceImplementation - BookCreation###");
        book.setBookAuthorId(authorId);
        log.info("Book to be updated: "+book.toString());
        return new ResponseEntity(this.bookrepository.save(book), HttpStatus.OK);
    }

    public ResponseEntity<?> bookUpdate(Book book, Integer authorId, Integer bookId) {
    	log.info("###BookServiceImplementation - BookUpdate###");
        return new ResponseEntity(this.bookrepository.save(book), HttpStatus.OK);
    }

    public ResponseEntity<?> searchBook(String category, String title, String author, Double price, String publisher) {
    	log.info("###BookServiceImplementation - SearchBook###");
    	UserResponse userResponse=new UserResponse();
        if(this.bookrepository.bookSearchCount(category, title, author, price, publisher) >= 1 ) { 
        	userResponse.setMessage(this.bookrepository.bookSearch(category, title, author, price, publisher).toString());
        		 }
        else{
        	userResponse.setMessage("No matching book found");}
        return new ResponseEntity(userResponse, HttpStatus.OK);
    }

    public java.util.List<Book> bookFetchAllSubscribe(Integer bookId) {
    	log.info("###BookServiceImplementation - FetchAllSubscribeBook###");
    	UserResponse userResponse=new UserResponse();
    	List<Book> b=new ArrayList<>(); 
    	SubscribeBook sb=new SubscribeBook();
    	for(int i=0;i<this.booksrepository.bookFetch(bookId).size();i++) {
    		Book b1=this.bookrepository.bookById(this.booksrepository.bookFetch(bookId).get(i).getBookId());
    		if(this.booksrepository.bookFetch(bookId).get(i).getBookSubscribe().equals("yes")) {
      b.add(b1);
    		}
    	}
    	return b;
    	/*}
        if (this.bookrepository.bookFetchCheck(bookId) != 0) {
        	
        	userResponse.setMessage(this.booksrepository.bookFetchC(bookId,aId).toString());
        	return new ResponseEntity(userResponse, HttpStatus.OK);
        } else {
        	userResponse.setMessage("No Book match found with the provided details");
            return new ResponseEntity(userResponse, HttpStatus.OK);
        }*/
    }

    public ResponseEntity<?> bookSubscribing(Integer bookId,Integer aId) {
    	log.info("###BookServiceImplementation - bookSubscribing###");
    	Random random = new Random();
        int num = random.nextInt(100000);
        String formatted = String.format("%05d", num);
        UserResponse userResponse=new UserResponse();
        if(this.booksrepository.bookFetchC(bookId, aId)==0) {
    	this.booksrepository.bookSubscribe(bookId,formatted,LocalDate.now(),"yes",aId);
    	userResponse.setMessage("Book is subscribed");}
        else if(this.booksrepository.bookFetchC(bookId, aId)==1) {
        	this.booksrepository.bookSubscribeU(aId,bookId,LocalDate.now(),"yes");
        	userResponse.setMessage("Book is subscribed again");
        }
        else { 
        	userResponse.setMessage("Book is already subscribed");
        }
    	return new ResponseEntity(userResponse, HttpStatus.OK);
    }

	/*@Override
	public ResponseEntity<?> bookFetchSubscribe(Integer authorId, String subscriptionId) {
		log.info("###BookServiceImplementation - FetchOneSubscribeBook###");
	        if (this.booksrepository.bookFetchCheck(authorId,subscriptionId) != 0) {
	            return new ResponseEntity(this.booksrepository.bookFetch(authorId,subscriptionId), HttpStatus.OK);
	        } else {
	            return new ResponseEntity("No Book match found with the provided details", HttpStatus.BAD_REQUEST);
	        }
	}*/

	@Override
	public ResponseEntity<?> cancelSubscriptionBook(Integer emailId, String subscriptionId) {
		// TODO Auto-generated method stub
		log.info("###BookServiceImplementation - cancelSubscriptionBook###");
		 UserResponse userResponse=new UserResponse();
		//if (this.booksrepository.bookFetchCheck(emailId,subscriptionId) != 0) {
			long j=this.booksrepository.bookFetchCAll(emailId,Integer.parseInt(subscriptionId)).getSubscriptionDate().getTime();
	
			long difference_In_Time = new Date().getTime()-j;
			 long difference_In_hours= (difference_In_Time/	 (1000 * 60 * 60));
			 if(difference_In_hours<24) {
			 booksrepository.bookUSubscribe(emailId,Integer.parseInt(subscriptionId),LocalDate.now(),"no");
		  userResponse.setMessage("Subscription cancelled");
            return new ResponseEntity<>(userResponse, HttpStatus.OK);
			 }
			 else {
				 userResponse.setMessage("You cant cancel the subscription now.Since 24hrs is crossed!!");
				 return new ResponseEntity(userResponse, HttpStatus.BAD_REQUEST);
			 }
        
	}

	@Override
	public ResponseEntity<?> bookFetchSubscribe(Integer authorId, String subscriptionId) {
		// TODO Auto-generated method stub
		return null;
	}
}
