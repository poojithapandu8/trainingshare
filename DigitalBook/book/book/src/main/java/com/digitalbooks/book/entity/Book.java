package com.digitalbooks.book.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Date;
import java.util.Optional;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.*;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bookId")
    private Integer bookId;
    private String bookTitle;
    @NotNull
    @Column(name = "bookAuthorId")
    private Integer bookAuthorId;
    @Enumerated(EnumType.STRING)
    private BookCategory bookCategory;
    private String bookPublisher;
    private Double bookPrice;
    private String bookContent;
    private Boolean bookStatus;
   // private String bookSubscribe;
   // private Date bookSubscribeDate;
  //  private String bookSubscribeId;
   // @JsonFormat(pattern = "dd/MM/yyyy")
    @DateTimeFormat(style = "dd/mm/yyyy")
    private LocalDate bookReleaseDate;
    private String bookImage;
    
   // @ManyToMany(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
   // @JoinTable(name="book_images",joinColumns= { @JoinColumn(name="book_id")},inverseJoinColumns= {@JoinColumn(name="id")})
    //private Set<ImageModel> bookImage;
	
		
}
