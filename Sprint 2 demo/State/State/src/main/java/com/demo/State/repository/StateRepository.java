package com.demo.State.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.State.entity.State;

@Repository
public interface StateRepository extends JpaRepository<State, Integer> {
	State findByStateId(Integer id);
}
