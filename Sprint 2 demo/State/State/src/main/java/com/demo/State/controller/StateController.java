package com.demo.State.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.State.entity.State;
import com.demo.State.service.StateService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("/state")
public class StateController {

	
	@Autowired
	StateService stateService;
	
	@PostMapping("/")
	public State saveState(@RequestBody State d) {
		log.info("###Controlelr - saveState()###");
		return stateService.saveState(d);
	}
	
	@GetMapping("/{d}")
	public State findStateById(@PathVariable Integer d) {
		log.info("###Controlelr - stateService ###");
		return stateService.findStateById(d);
	}
	
	
	
	@GetMapping("/home")
	public String home() {
		log.info("###Controlelr - stateService ###");
		return "Welcome State Home page !!";
	}
}
