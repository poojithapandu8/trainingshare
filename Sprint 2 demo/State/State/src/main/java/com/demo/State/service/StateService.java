package com.demo.State.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.State.entity.State;
import com.demo.State.repository.StateRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class StateService {

	@Autowired
	StateRepository  stateRepository;
	
	public State saveState(State d) {
		log.info("###Service - saveDepartment()###");
		return stateRepository.save(d);
	}
	
	public State findStateById(Integer d) {
		log.info("###Service - findDepartmentById()###");
		return stateRepository.findByStateId(d);
	}

}
