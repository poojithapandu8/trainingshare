package com.demo.State;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import com.demo.State.entity.State;
import com.demo.State.repository.StateRepository;

@SpringBootApplication
@EnableEurekaClient
public class StateApplication implements CommandLineRunner{

	@Autowired
	StateRepository stateRepository;
	public static void main(String[] args) {
		SpringApplication.run(StateApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		State s=new State(1,"Delhi","2000");
		State s1=new State(2,"Washington DC","23000");
		State s2=new State(3,"London","13000");
		State s3=new State(4,"Rome","400");
		stateRepository.save(s);
		stateRepository.save(s1); 
		stateRepository.save(s2);
		stateRepository.save(s3);
	}

}
