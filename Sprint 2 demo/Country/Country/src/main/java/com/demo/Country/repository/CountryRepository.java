package com.demo.Country.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.demo.Country.entity.Country;

@Repository
public interface CountryRepository extends JpaRepository<Country, Integer> {
	Country findByCountryId(Integer countryId);
	
	@Query(value="select count(*) from country where country_Id=?1",nativeQuery=true)
	Integer findtheCId(Integer id);
}
