package com.demo.Country.service;


import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.demo.Country.VO.ResponseTemplateVO;
import com.demo.Country.VO.State;
import com.demo.Country.entity.Country;
import com.demo.Country.exception.BusinessException;
import com.demo.Country.repository.CountryRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CountryService {
	
	@Autowired
	private RestTemplate restTempalte;
	
	@Autowired
	CountryRepository countryRepository;
	
	public Country saveUser(Country u) {
		
		return countryRepository.save(u);
	}
	
	public ResponseTemplateVO getCountryWithState(Integer userId) {
		ResponseTemplateVO rt=new ResponseTemplateVO();
		Set<State> sList=new HashSet<>();
		Country c=countryRepository.findByCountryId(userId);
		log.info("#### lsit of country findall : "+countryRepository.findAll());
		List<Country> lCountry=countryRepository.findAll().stream().filter(b->b.getCountryId()==userId).collect(Collectors.toList());
		//List<State> lState=c.getStateId().stream().filter(b->).collect(Collectors.toList());
		log.info("#### lsit of country : "+lCountry);
		for(Country c1 : lCountry) {
		State s=restTempalte.getForObject("http://localhost:5555/state/"+c1.getCstateId(), State.class);
		log.info("In ResponseTemplate class : "+s);
		sList.add(s);
		}
		log.info("#### lsit of state : "+sList);
		rt.setCountryDetails(c);
		rt.setStateDetails(sList);
		return rt;	
		
	}

	public List<Country> getAllCountryDetaisl(Integer id) {
		// TODO Auto-generated method stub
		log.info("Status of id : "+countryRepository.findtheCId(id));
		
		try {
			if(countryRepository.findtheCId(id)!=0) {
			return countryRepository.findAll(); 
			}
			else {
				log.info("exception block");
				throw new Exception();
			}
		}
		catch(Exception e) {
			throw new BusinessException("611","Not Id found for this customer");
		}
		
		/*
		 * if(countryRepository.findtheCId(id)==0) { return new ResponseEntity<>(new
		 * BusinessException("611","Not Id found for this customer"),HttpStatus.
		 * BAD_REQUEST); } else { return new
		 * ResponseEntity<>(countryRepository.findAll(),HttpStatus.OK); }
		 */
}
}
