package com.demo.Country.VO;

import java.util.List;
import java.util.Set;

import com.demo.Country.entity.Country;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseTemplateVO {
	
	private Country countryDetails;
	private Set<State> stateDetails;

}
