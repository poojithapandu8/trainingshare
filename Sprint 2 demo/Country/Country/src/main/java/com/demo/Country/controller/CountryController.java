package com.demo.Country.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.Country.VO.ResponseTemplateVO;
import com.demo.Country.entity.Country;
import com.demo.Country.service.CountryService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("/country")
public class CountryController {
	@Autowired
	CountryService countryService;
	
	@GetMapping("/home")
	public String home() {
		log.info("###Controller - home page()###");
		return "Welcome to Country App!!";
	}
	
	@GetMapping("/id/{id}")
	public List<Country> home(@PathVariable Integer id) {
		log.info("###Controller - home page()###");
		return countryService.getAllCountryDetaisl(id);
	}
	
	@PostMapping("/")
	public Country saveCountry(@RequestBody Country d) {
		log.info("###Controller - saveUser()###");
		return countryService.saveUser(d);
	}
	
	@GetMapping("/{d}")
	public ResponseTemplateVO getCountryWithState(@PathVariable Integer d) {
		log.info("###Controller - getUserWithDepartment###");
		return countryService.getCountryWithState(d);
		
	}
	
}
