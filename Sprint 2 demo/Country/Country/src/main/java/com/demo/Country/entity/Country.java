package com.demo.Country.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.demo.Country.VO.State;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
//@IdClass(Country.class)

//@Table(uniqueConstraints= {@UniqueConstraint(columnNames= {"country_id","state_id"})})
public class Country  {

	
	@Id
	private int countryId;
	private String countryName;
	private int countryPopulation;
	private List<State> cstateId;
}
