export class BookC {
   public bookId!:number;
    bookTitle!:string;
    bookAuthorId!:number;
     bookCategory!:string;
    bookPublisher!:string;
    bookPrice!:number;
     bookContent!:string;
     bookStatus!:boolean;
     bookReleaseDate!:Date;
     bookImage!:string;
}
