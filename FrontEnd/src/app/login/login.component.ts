import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { User } from '../user';
import {ServiceService} from '../service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
user=new User();
images_d="/assets/images/ebook1.jpg";
  constructor(private _route: Router,private service:ServiceService) { }

  ngOnInit(): void {
  }
  signUpPage(){
    this._route.navigate(['/signup']);
  }
  jwtuserlogin(){
    localStorage.clear();
    console.log("In JWT authenticaiton process")
    console.log(this.user.userEmailId);
    this.service.jwtauthenticationCheck(this.user).subscribe(data => {
   console.log(data.token+":"+this.user);
   localStorage.setItem('jwt_token', JSON.stringify(data.token));
   localStorage.setItem('UserEmail', JSON.stringify(this.user.userEmailId));
   localStorage.setItem('UserId', JSON.stringify(data.message));
   localStorage.setItem('URole', JSON.stringify(data.urole));
   console.log("UserId"+JSON.parse(localStorage.getItem('UserId')));
   console.log("jwt get :"+JSON.parse(localStorage.getItem('jwt_token'))+"role:"+JSON.parse(localStorage.getItem('URole')));
   alert("Login Sucesfull");
   if(JSON.parse(localStorage.getItem('URole'))=="ROLE_AUTHOR"){
    this._route.navigate(['/home']);}
    else{
     this._route.navigate(['/books']);
    }
  }, error=>alert("Error : Login failed. Please try again!!"));

  //this.service.homePage().subscribe(data=>{console.log(data)},error=>alert("jwt error"));
  }

  userlogin(){
    console.log("In authenticaiton process")
    console.log(this.user.userEmailId);
    this.service.authenticationCheck(this.user).subscribe(data => {//alert("Login Sucesfull");
   this._route.navigate(['/home']);
   console.log(data);
  }, error=>alert("Error : Login failed. Please try again!!"));
  }
}
