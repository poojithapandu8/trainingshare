import { Component, OnInit, Optional ,Inject} from '@angular/core';
import {MatDialog,MatDialogRef,MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatDialogModule} from '@angular/material/dialog';
import {BookComponent} from '../book/book.component';
import {Router} from '@angular/router';
import { BookC } from '../book-c';
import { ServiceService } from '../service.service';
import {AfterViewInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { MatChipsModule } from '@angular/material/chips';
import { ViewBookComponent } from '../view-book/view-book.component';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {   
  book: Array<BookC> = [];
  b1:BookC=new BookC();
  displayedColumns: string[] = ['id', 'fullName', 'email', 'entryType','streamType','action'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  constructor( private _route: Router,private _service:ServiceService,
    @Optional() private dialogRef:MatDialogRef<BookComponent>,
    @Optional() private dialog:MatDialog) { }
   
  ngOnInit(): void {
   // this.home();
    this.getStudents();
  }

  home(){
    this._service.homePage().subscribe(data=>{console.log(data)},error=>alert("jwt error"));
  }
  openBook(){
    console.log("Opening Book");
    this._route.navigate(['book']);
  }
  editStudent(row:any){
    console.log("Editing the data");
    this._route.navigate(['editBook/'+row]);
  }
  viewBooks(row:any){
    this._route.navigate(['viewBook/'+row]);
  }search(){
    this._route.navigate(['/search']);
  }
  blockBooks(row:any,s:any){
    this._service.blockBook(JSON.parse(localStorage.getItem('UserId')),row,s,this.b1).subscribe(
      {
        next:(res)=>{
          console.log("block/Unblock book"+res+row+s);
          if(s=="yes"){
          alert(" Book blocked successfully!!");}
          else{
            alert(" Book unblocked successfully!!");
          }
        },error:(err)=> { 
          alert("Exception occurred while blocking/unblocking book")}
      })
  }
  getStudents() {
    this._service.allByBookDetails(JSON.parse(localStorage.getItem('UserId'))).subscribe(
       {
         next:(res)=>{
           this.dataSource=new MatTableDataSource(res)
           this.dataSource.paginator=this.paginator
           this.dataSource.sort=this.sort
         },error:(err)=> { 
           alert("Exception occurred 1")}
       })
      }
      logout(){
        this._route.navigate(['/login']);
      }
      allBooks(){
        this._route.navigate(['/books']);
      }
  isEmpty()
  {
    if (this.book == null)
    {
      return true;
    }
    else { return false; }
  }
  }
