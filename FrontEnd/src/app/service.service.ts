import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders,HttpResponse } from '@angular/common/http';
import {Observable} from 'rxjs';
import { AnyCatcher } from 'rxjs/internal/AnyCatcher';
import { User } from './user';
import { BookC } from './book-c';

var headers_object = new HttpHeaders();
headers_object.append('Content-Type', 'application/json');
headers_object.append("Authorization", "Bearer " + JSON.parse(localStorage.getItem('jwt_token')));

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json','Authorization':'Bearer ' + JSON.parse(localStorage.getItem('jwt_token'))})
};

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  constructor(private _http:HttpClient) { }
 // uapi_url='http://localhost:8185';
 // bapi_url='http://localhost:8181';
  uapi_url='http://Userms-env.eba-shawa3yn.ap-northeast-1.elasticbeanstalk.com';
  bapi_url='http://Bookms-env.eba-jcrptkfp.ap-northeast-1.elasticbeanstalk.com';
  homePage():Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({ 
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Headers': 'Content-Type',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',
                'key': 'x-api-key',
                'Authorization':'Bearer ' + JSON.stringify(localStorage.getItem('jwt_token')),})
    };
    console.log("Output from jwt"+JSON.stringify(localStorage.getItem('jwt_token'))+"headers:"+httpOptions.headers.get('Content-Type'));
    //httpOptions.headers.set('Content-Type','application/json');
    //httpOptions.headers.set('Authorization','Bearer '+JSON.parse(localStorage.getItem('jwt_token')));
    console.log("check again:"+httpOptions.headers.get('Access-Control-Allow-Origin'));
    return this._http.get<any>(this.uapi_url+'/homeR',httpOptions);
  }
  
  jwtauthenticationCheck(u:User):Observable<any>{
    console.log("Output from "+u);
    return this._http.post<any>(this.uapi_url+'/authenticate',u);
  }

  authenticationCheck(u:User):Observable<any>{
    console.log("Output from "+u);
    return this._http.post<any>(this.uapi_url+'/api/v1/digitalbooks/signin',u);
  }
  signupUser(u:User):Observable<any>{
    return this._http.post<any>(this.uapi_url+'/api/v1/digitalbooks/signup',u);
  }

  allBookDetails():Observable<any>{
    return this._http.get<any>(this.bapi_url+'/api/v1/digitalbooks/all');
  }

  allByBookDetails(id:number):Observable<any>{
    return this._http.get<any>(this.bapi_url+'/api/v1/digitalbooks/all/'+id);
  }

  viewBook(id:number):Observable<any>{
    return this._http.get<any>(this.bapi_url+'/api/v1/digitalbooks/book/'+id);
  }

  editBook(id:number,b:BookC):Observable<any>{
    return this._http.put<any>(this.bapi_url+'/api/v1/digitalbooks/book/'+id,b);
  }

  createBook(id:number,b:BookC):Observable<any>{
    return this._http.post<any>(this.bapi_url+'/api/v1/digitalbooks/author/'+id+'/books',b);
  }

  blockBook(id:string,idB:number,s:string,b:BookC):Observable<any>{
    return this._http.post<any>(this.bapi_url+'/api/v1/digitalbooks/author/'+id+'/book/'+idB+'?block='+s,b);
  }

  searchBook(b:BookC):Observable<any>{
    return this._http.get<any>(this.bapi_url+'/api/v1/digitalbooks/search?category='+b.bookCategory+'&title='+b.bookTitle+'&author='+b.bookAuthorId+'&price='+b.bookPrice+'&publisher='+b.bookPublisher);
  }

}
