import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders,HttpResponse } from '@angular/common/http';
import {Observable} from 'rxjs';
import { AnyCatcher } from 'rxjs/internal/AnyCatcher';
import { User } from './user';
import { BookC } from './book-c';

@Injectable({
  providedIn: 'root'
})
export class ReadersserviceService {
 // uapi_url='http://localhost:8185';
 // bapi_url='http://localhost:8181';
 uapi_url='http://Userms-env.eba-shawa3yn.ap-northeast-1.elasticbeanstalk.com';
 bapi_url='http://Bookms-env.eba-jcrptkfp.ap-northeast-1.elasticbeanstalk.com';
  constructor(private _http:HttpClient) { }

  allRSBookDetails():Observable<any>{
    return this._http.get<any>(this.bapi_url+'/api/v1/digitalbooks/readers/'+JSON.parse(localStorage.getItem('UserId'))+'/books');
  }

  allRBookDetails():Observable<any>{
    return this._http.get<any>(this.bapi_url+'/api/v1/digitalbooks/all');
  }

  subscribeRBook(id:number,b:BookC):Observable<any>{
    return this._http.post<any>(this.bapi_url+'/api/v1/digitalbooks/'+id+'/subscribe?aId='+JSON.parse(localStorage.getItem('UserId')),b);
  }
  usubscribeRBook(emailid:number,sid:string,b:BookC):Observable<any>{
    return this._http.post<any>(this.bapi_url+'/api/v1/digitalbooks/readers/'+emailid+'/books/'+sid+'/cancel-subscription',b);
  }
}
