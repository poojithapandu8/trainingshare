import { Component, OnInit , Optional ,Inject} from '@angular/core';
import {MatDialog,MatDialogRef,MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatDialogModule} from '@angular/material/dialog';
import {BookComponent} from '../book/book.component';
import {Router} from '@angular/router';
import { BookC } from '../book-c';
import { ServiceService } from '../service.service';
import {AfterViewInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { MatChipsModule } from '@angular/material/chips';
import { ViewBookComponent } from '../view-book/view-book.component';
import { ReadersserviceService } from '../readersservice.service';

@Component({
  selector: 'app-readers-dashboard',
  templateUrl: './readers-dashboard.component.html',
  styleUrls: ['./readers-dashboard.component.scss']
})
export class ReadersDashboardComponent implements OnInit {
  book: Array<BookC> = [];
  b1:BookC=new BookC();
  displayedColumns: string[] = ['id', 'fullName', 'email', 'entryType','streamType','subscriptionId','action'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  constructor( private _route: Router,private _service:ServiceService,
    private service:ReadersserviceService,
    @Optional() private dialogRef:MatDialogRef<BookComponent>,
    @Optional() private dialog:MatDialog) { }

  ngOnInit(): void {
    this.getSBooks();
  }

  usBook(id:string){
    console.log(id);
    this.service.usubscribeRBook(JSON.parse(localStorage.getItem('UserId')),id,this.b1).subscribe(
      {
        next:(res)=>{
         console.log(res);
          alert(res.body.message)
        },error:(err)=> { 
          alert("Not able to unsubscribe book!!!")}
      })
  }
  viewBooks(row:any){
    this._route.navigate(['viewBook/'+row]);
  }
  openBook(){
    console.log("Opening Book");
    this._route.navigate(['book']);
  }
  logout(){
    this._route.navigate(['/login']);
  }
  search(){
    this._route.navigate(['/search']);
  }
  getSBooks() {
    this.service.allRSBookDetails().subscribe(
       {
         next:(res)=>{
          console.log(res);
           this.dataSource=new MatTableDataSource(res)
           this.dataSource.paginator=this.paginator
           this.dataSource.sort=this.sort
         },error:(err)=> { 
           alert("Not able to view the data")}
       })
      }
      allBooks(){
        this._route.navigate(['/books']);
      }
}
