import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadersDashboardComponent } from './readers-dashboard.component';

describe('ReadersDashboardComponent', () => {
  let component: ReadersDashboardComponent;
  let fixture: ComponentFixture<ReadersDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReadersDashboardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReadersDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
