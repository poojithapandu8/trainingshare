import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookComponent } from './book/book.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ViewBookComponent } from './view-book/view-book.component';
import { EditBookComponent } from './edit-book/edit-book.component';
import { AllbooksComponent } from './allbooks/allbooks.component';
import { SearchComponent } from './search/search.component';
import { ReadersDashboardComponent } from './readers-dashboard/readers-dashboard.component';

const routes: Routes = [
  {path:'',component:LoginComponent},
  {path:'login',component:LoginComponent},
  {path:'signup',component:SignupComponent},
  {path:'home',component:HomeComponent},
  {path:'book',component:BookComponent},
  {path:'viewBook/:row',component:ViewBookComponent},
  {path:'readersDashboard',component:ReadersDashboardComponent},
  {path:'editBook/:row',component:EditBookComponent},
  {path:'books',component:AllbooksComponent},
  {path:'search',component:SearchComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
