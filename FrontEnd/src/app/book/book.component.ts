import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';
import {NgForm} from '@angular/forms';
import { ServiceService } from '../service.service';
import {Router,ActivatedRoute} from '@angular/router';
import {User} from '../user';
import { HttpClient,HttpEventType } from '@angular/common/http';
import {MatDialog,MatDialogModule,MatDialogRef,MAT_DIALOG_DATA} from '@angular/material/dialog';
import { BookC } from '../book-c';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit {
  bookF!:FormGroup;
  book=new BookC();
  u=new User();
  selectedFile: File;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  message: string;
  imageName: any;
  // uapi_url='http://localhost:8185';
 // bapi_url='http://localhost:8181';
 uapi_url='http://Userms-env.eba-shawa3yn.ap-northeast-1.elasticbeanstalk.com';
 bapi_url='http://Bookms-env.eba-jcrptkfp.ap-northeast-1.elasticbeanstalk.com';
  constructor(private _route: Router,private _service:ServiceService,
    private formBuilder:FormBuilder,private routeA:ActivatedRoute,private http:HttpClient) { }

  ngOnInit(): void {
    
    this.bookF=this.formBuilder.group({
      bookId:['',Validators.required],
      bookTitle:['',Validators.required],
      bookAuthorId:['',Validators.required],
      bookPublisher:['',Validators.required],
      bookCategory:['',Validators.required],
      bookContent:['',Validators.required],
      bookPrice:['',Validators.required],
      bookStatus:['',Validators.required],
      bookSubscribe:['',Validators.required],
      bookSubscribeId:['',Validators.required],
      bookSubscribeDate:['',Validators.required],
      bookReleaseDate:['',Validators.required],
      bookImage:['',Validators.required],
    }); 
  }
  addbook(){
   this.book=this.bookF.value
   //var d=new Date(this.bookF.controls['bookTitle']);
   // this.bookF.controls['bookReleaseDate'].setValue(this.book.bookReleaseDate.getDate()+"-"+this.book.bookReleaseDate.getMonth()+"-"+this.book.bookReleaseDate.getFullYear());
    this.book=this.bookF.value
   // let parsedDate = moment(this.book.bookReleaseDate,"YYYY-MM-DD");
//let outputDate = parsedDate.format("DD-MM-YYYY");
let a=this.book.bookReleaseDate.getDate()+"-"+this.book.bookReleaseDate.getMonth()+"-"+this.book.bookReleaseDate.getFullYear();
const d=new Date(a);
    console.log(d.toLocaleDateString()+":date:"+d.toJSON()+":"+d.toDateString()+":"+d.toLocaleString());
    this.bookF.controls['bookReleaseDate'].setValue(d.toJSON().split('T')[0]);
this._service.createBook(1,this.bookF.value).subscribe(
  {
    next:(res)=>{
      this.book=res;
       console.log(this.bookF.controls['bookTitle']+":"+this.book.bookReleaseDate+":"+res);
      console.log("Book is saved sucessfully!!");
      alert("Book is saved sucessfully!!");
      this._route.navigate(['/home']);
    },error:(err)=> { 
      alert("Error while creating book!!")}
  })
  }
  homPage(){
this._route.navigate(['/home']);
  }
  logout(){
    this._route.navigate(['/login']);
  }
  search(){
    this._route.navigate(['/search']);
  }
  onFileSelected(event){
    this.selectedFile=event.target.files[0]
console.log(event);
  }
  onUpload(){
    console.log(this.selectedFile);
   const uploadImageData = new FormData();
    uploadImageData.append('imageFile', this.selectedFile, this.selectedFile.name);
    console.log(uploadImageData.get('imageFile'));
    this.bookF.controls['bookImage'].setValue(this.selectedFile.name);
    this.http.post(this.bapi_url+'/image/upload', uploadImageData, { observe: 'response' })
      .subscribe((response) => {
        if (response.status === 200) {
          this.message = 'Image uploaded successfully';
        } else {
          this.message = 'Image not uploaded successfully';
        }
      }
      );
  }

  getImage() {
    this.http.get(this.bapi_url+'/image/get/' + this.imageName)
      .subscribe(
        res => {
          this.retrieveResonse = res;
          this.base64Data = this.retrieveResonse.picByte;
          this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
        }
      );
  }
}
