import { Component, OnInit , Optional ,Inject} from '@angular/core';
import {MatDialog,MatDialogRef,MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatDialogModule} from '@angular/material/dialog';
import {BookComponent} from '../book/book.component';
import {Router} from '@angular/router';
import { BookC } from '../book-c';
import { ServiceService } from '../service.service';
import {AfterViewInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { MatChipsModule } from '@angular/material/chips';
import { ViewBookComponent } from '../view-book/view-book.component';
import { ReadersserviceService } from '../readersservice.service';

@Component({
  selector: 'app-allbooks',
  templateUrl: './allbooks.component.html',
  styleUrls: ['./allbooks.component.scss']
})
export class AllbooksComponent implements OnInit {

  book: Array<BookC> = [];
  b1:BookC=new BookC();
  displayedColumns: string[] = ['id', 'fullName', 'email', 'entryType','streamType','action'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  constructor( private _route: Router,private _service:ServiceService,
    private service:ReadersserviceService,
    @Optional() private dialogRef:MatDialogRef<BookComponent>,
    @Optional() private dialog:MatDialog) { }

  ngOnInit(): void {
    this.allBooks();
  }
  logout(){
    this._route.navigate(['/login']);
  }

  sBook(id:number){
    console.log(id);
    this.service.subscribeRBook(id,this.b1).subscribe(
      {
        next:(res)=>{
         console.log(res);
          alert("subscribed book sucessfully!!!")
        },error:(err)=> { 
          alert("Not able to subscribe book")}
      })
  }

  getSBooks() {
    this._route.navigate(['/readersDashboard']);
  }
  allBooks(){
    this.service.allRBookDetails().subscribe(
      {
        next:(res)=>{
         console.log(res.body);
          this.dataSource=new MatTableDataSource(res)
          this.dataSource.paginator=this.paginator
          this.dataSource.sort=this.sort
        },error:(err)=> { 
          alert("Not able to view the data")}
      })
  }
  viewBooks(row:any){
    this._route.navigate(['viewBook/'+row]);
  }
  search(){
    this._route.navigate(['/search']);
  }
}
