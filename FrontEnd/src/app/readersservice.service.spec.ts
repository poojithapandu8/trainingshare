import { TestBed } from '@angular/core/testing';

import { ReadersserviceService } from './readersservice.service';

describe('ReadersserviceService', () => {
  let service: ReadersserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReadersserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
