import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';
import {NgForm} from '@angular/forms';
import { ServiceService } from '../service.service';
import { BookC } from '../book-c';
import { HttpClient,HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-view-book',
  templateUrl: './view-book.component.html',
  styleUrls: ['./view-book.component.scss']
})
export class ViewBookComponent implements OnInit {
  bookF!:FormGroup;
  book1=new BookC();
  selectedFile: File;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  message: string;
  imageName: any;
 // uapi_url='http://localhost:8185';
 // bapi_url='http://localhost:8181';
 uapi_url='http://Userms-env.eba-shawa3yn.ap-northeast-1.elasticbeanstalk.com';
 bapi_url='http://Bookms-env.eba-jcrptkfp.ap-northeast-1.elasticbeanstalk.com';
  constructor(private _route: Router,private _service:ServiceService,
    private formBuilder:FormBuilder,private routeA:ActivatedRoute,private http:HttpClient) { }

  ngOnInit(): void {
    this.bookF=this.formBuilder.group({
      bookId:['',Validators.required],
      bookTitle:['',Validators.required],
      bookAuthorId:['',Validators.required],
      bookPublisher:['',Validators.required],
      bookCategory:['',Validators.required],
      bookContent:['',Validators.required],
      bookPrice:['',Validators.required],
      bookStatus:['',Validators.required],
      bookReleaseDate:['',Validators.required],
      bookImage:['',Validators.required],
    });
    let id=this.routeA.snapshot.paramMap.get('row');
   // var a:number= +id;
   this.getImage();
    this.viewBooks(parseInt(id));
  }
  logout(){
    this._route.navigate(['/login']);
  }
  getImage() {
    this.http.get(this.bapi_url+'/image/get/' + JSON.parse(localStorage.getItem('bookimage')))
      .subscribe(
        res => {
          this.retrieveResonse = res;
          this.base64Data = this.retrieveResonse.picByte;
          this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
          console.log("Viewing iamge");
        }
      );
  }
  homPage(){
    if(JSON.parse(localStorage.getItem('URole'))=="ROLE_AUTHOR"){
      this._route.navigate(['/home']);}
      else{
       this._route.navigate(['/readersDashboard']);
      }
      }
      viewBooks(id:number){
        this._service.viewBook(id).subscribe(
          {
            next:(res)=>{
              this.book1=res;
              console.log("view Page"+res+":"+this.book1.bookCategory);
              this.bookF.controls['bookId'].setValue(this.book1.bookId);
            this.bookF.controls['bookTitle'].setValue(this.book1.bookTitle);
            this.bookF.controls['bookPublisher'].setValue(this.book1.bookPublisher);
            this.bookF.controls['bookCategory'].setValue(this.book1.bookCategory);
            this.bookF.controls['bookContent'].setValue(this.book1.bookContent);
            this.bookF.controls['bookPrice'].setValue(this.book1.bookPrice);
            this.bookF.controls['bookReleaseDate'].setValue(this.book1.bookReleaseDate);
            this.bookF.controls['bookStatus'].setValue(this.book1.bookStatus);
            this.bookF.controls['bookAuthorId'].setValue(this.book1.bookAuthorId);
            this.bookF.controls['bookImage'].setValue(this.book1.bookImage);
            localStorage.setItem('bookimage', JSON.stringify(this.book1.bookImage));
            console.log("Image name:"+ localStorage.getItem('bookimage'));
            },error:(err)=> { 
              alert("Exception occurred 1")}
          })
      }
}
