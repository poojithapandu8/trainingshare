import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,Validator,Validators} from '@angular/forms';
import { ServiceService } from '../service.service';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {User} from '../user';
import {MatDialog,MatDialogRef,MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  userF!:FormGroup;
  user=new User();
  constructor(private formBuilder:FormBuilder,private _route: Router,
    private _service: ServiceService) { }

  ngOnInit(): void {
    this.userF=this.formBuilder.group({
      userName:['',Validators.required],
      userEmailId:['',Validators.required],
      password:['',Validators.required],
      role:['',Validators.required]
    });
    console.log("Data ");
  }
  loginPage(){
    this._route.navigate(['/login']);
  }
  signupU(){
    console.log("Data saving");
    this._service.signupUser(this.userF.value).subscribe
    (
      data =>{
       alert("Account created successfully!!!");
       this.userF.reset();
       
      },
      error =>console.log("Error")
    )
  }
}
