import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {MatCardModule} from '@angular/material/card';
import { AppRoutingModule } from './app-routing.module';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import { AppComponent } from './app.component';
import {MatPaginator} from '@angular/material/paginator';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import {MatDialog,MatDialogRef,MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormBuilder,FormGroup,Validator,Validators} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatTabsModule} from '@angular/material/tabs';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatInputModule} from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import { LoginComponent } from './login/login.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import { SignupComponent } from './signup/signup.component';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { BookComponent } from './book/book.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';
import { ViewBookComponent } from './view-book/view-book.component';
import { EditBookComponent } from './edit-book/edit-book.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {DateAdapter, MatNativeDateModule} from '@angular/material/core';
import { ReadersDashboardComponent } from './readers-dashboard/readers-dashboard.component';
import { AllbooksComponent } from './allbooks/allbooks.component';
import { SearchComponent } from './search/search.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    HomeComponent,
    BookComponent,
    ViewBookComponent,
    EditBookComponent,
    ReadersDashboardComponent,
    AllbooksComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,MatTabsModule,MatFormFieldModule,MatSelectModule,FormsModule,FormsModule,ReactiveFormsModule,
    AppRoutingModule,MatButtonModule,HttpClientModule,MatToolbarModule,MatTableModule,
    MatCardModule, MatIconModule,BrowserAnimationsModule,MatInputModule,MatCheckboxModule,
    MatRadioModule,MatPaginatorModule,MatDatepickerModule,MatNativeDateModule,MatTooltipModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
