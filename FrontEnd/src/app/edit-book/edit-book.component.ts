import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';
import {NgForm} from '@angular/forms';
import { ServiceService } from '../service.service';
import { BookC } from '../book-c';
import { HttpClient,HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.scss']
})
export class EditBookComponent implements OnInit {
  bookF!:FormGroup;
  book1=new BookC();
  selectedFile: File;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  message: string;
  imageName: any;
  // uapi_url='http://localhost:8185';
 // bapi_url='http://localhost:8181';
 uapi_url='http://Userms-env.eba-shawa3yn.ap-northeast-1.elasticbeanstalk.com';
 bapi_url='http://Bookms-env.eba-jcrptkfp.ap-northeast-1.elasticbeanstalk.com';
  constructor(private _route: Router,private _service:ServiceService,
    private formBuilder:FormBuilder,private routeA:ActivatedRoute,private http:HttpClient) { }

  ngOnInit(): void {
    this.bookF=this.formBuilder.group({
      bookId:['',Validators.required],
      bookTitle:['',Validators.required],
      bookAuthorId:['',Validators.required],
      bookPublisher:['',Validators.required],
      bookCategory:['',Validators.required],
      bookContent:['',Validators.required],
      bookPrice:['',Validators.required],
      bookStatus:['',Validators.required],
      bookReleaseDate:['',Validators.required],bookImage:['',Validators.required],
    });
    let id=this.routeA.snapshot.paramMap.get('row');
    // var a:number= +id;
     this.viewBooks(parseInt(id));
  }
  homPage(){
    this._route.navigate(['/home']);
      }
      onFileSelected(event){
        this.selectedFile=event.target.files[0]
    console.log(event);
      }

      viewBooks(id:number){
        this._service.viewBook(id).subscribe(
          {
            next:(res)=>{
              this.book1=res;
              console.log("Before edit view Page"+res+":"+this.book1.bookCategory);
              this.bookF.controls['bookId'].setValue(this.book1.bookId);
            this.bookF.controls['bookTitle'].setValue(this.book1.bookTitle);
            this.bookF.controls['bookPublisher'].setValue(this.book1.bookPublisher);
            this.bookF.controls['bookCategory'].setValue(this.book1.bookCategory);
            this.bookF.controls['bookContent'].setValue(this.book1.bookContent);
            this.bookF.controls['bookPrice'].setValue(this.book1.bookPrice);
            this.bookF.controls['bookReleaseDate'].setValue(this.book1.bookReleaseDate);
            this.bookF.controls['bookStatus'].setValue(this.book1.bookStatus);
            this.bookF.controls['bookAuthorId'].setValue(this.book1.bookAuthorId);
            },error:(err)=> { 
              alert("EDit page dispaly error")}
          })
      }

      editBook(){
        this._service.editBook(this.book1.bookId,this.bookF.value).subscribe(
          {
            next:(res)=>{
              console.log(this.bookF.value);
              console.log(this.book1.bookId);
              //alert("Book is saved sucessfully!!");
              this._route.navigate(['/home']);
            },error:(err)=> { 
              alert("Editing book data error")}
          })
      }

      onUpload(){
        console.log(this.selectedFile);
       const uploadImageData = new FormData();
        uploadImageData.append('imageFile', this.selectedFile, this.selectedFile.name);
        console.log(uploadImageData.get('imageFile'));
        this.bookF.controls['bookImage'].setValue(this.selectedFile.name);
        this.http.post(this.bapi_url+'/image/upload', uploadImageData, { observe: 'response' })
          .subscribe((response) => {
            if (response.status === 200) {
              this.message = 'Image uploaded successfully';
            } else {
              this.message = 'Image not uploaded successfully';
            }
          }
          );
      }
      
}
