import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';
import {NgForm} from '@angular/forms';
import { ServiceService } from '../service.service';
import { BookC } from '../book-c';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  bookF!:FormGroup;
  book1=new BookC();
  constructor(private _route: Router,private _service:ServiceService,
    private formBuilder:FormBuilder,private routeA:ActivatedRoute) { }

  ngOnInit(): void {
    this.bookF=this.formBuilder.group({
      bookId:['',Validators.required],
      bookTitle:['',Validators.required],
      bookAuthorId:['',Validators.required],
      bookPublisher:['',Validators.required],
      bookCategory:['',Validators.required],
      bookContent:['',Validators.required],
      bookPrice:['',Validators.required],
      bookStatus:['',Validators.required],
      bookReleaseDate:['',Validators.required],bookImage:['',Validators.required],
    });
  }
  logout(){
    this._route.navigate(['/login']);
  }
  searchBook(){
    console.log(this.bookF.value);
    this._service.searchBook(this.bookF.value).subscribe(
      {
        next:(res)=>{
         console.log(res.message);
          alert("Searching book!!!"+res.message);
          this.bookF.reset();
          this.homPage();
        },error:(err)=> { 
          alert("Not able to search book!!!")}
      })
  }
  homPage(){
    if(JSON.parse(localStorage.getItem('URole'))=="ROLE_AUTHOR"){
      this._route.navigate(['/home']);}
      else{
       this._route.navigate(['/books']);
      }
      }
}
