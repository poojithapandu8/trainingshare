import { Component, OnInit,Inject } from '@angular/core';
import {FormBuilder,FormGroup,Validator,Validators} from '@angular/forms';
import { ServiceService } from '../service.service';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import { Student } from '../student';
import {MatDialog,MatDialogRef,MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
  studentD!:FormGroup;
  student =new Student();
  actionBtn: string="Save";
  constructor(private formBuilder:FormBuilder,private _route: Router,
    private _service: ServiceService,
    @Inject(MAT_DIALOG_DATA) public editData:any,
    private dialogRef:MatDialogRef<DialogComponent>) { }

  ngOnInit(): void {
    this.studentD=this.formBuilder.group({
      studentName:['',Validators.required],
      email:['',Validators.required],
      entryType:['',Validators.required],
      courseType:['',Validators.required],
      gradePoints:['',Validators.required],
    });
    if(this.editData){
      this.actionBtn="Update";
this.studentD.controls['studentName'].setValue(this.editData.studentName);
this.studentD.controls['email'].setValue(this.editData.email);
this.studentD.controls['entryType'].setValue(this.editData.entryType);
this.studentD.controls['courseType'].setValue(this.editData.courseType);
this.studentD.controls['gradePoints'].setValue(this.editData.gradePoints);
    }
    console.log("Data :"+this.studentD+":"+this.actionBtn);
  }
  formatLabel(value: number) {
    if (value >= 1000) {
      return Math.round(value / 10000) ;
    }
    return value;
  }

  editData1(){
    this._service.updateStudentList(this.studentD.value,this.editData.id).subscribe
    (
      data =>{
       alert("Edited successfully");
       this.studentD.reset();
       this.dialogRef.close();
       console.log("Edited successfully");
      },
      error =>console.log("Error")
    )
  }
  addStudent(){  
console.log(this.studentD.value);}

addCourseformsubmit(){
if(this.editData){
  console.log("Editing data");
  this.editData1();
}
else{
  console.log("Saving data");
  this._service.addStudentList(this.studentD.value).subscribe(
  data =>{
    console.log("Data added successfully"+data);
    this.dialogRef.close();
    this._service.fetchStudentList();
  },
  error =>console.log("Error")
)}}
}
