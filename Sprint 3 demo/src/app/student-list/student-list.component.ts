import { Component, Inject, OnInit } from '@angular/core';
import { Student } from '../student';
import {MatDialog,MatDialogRef,MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ServiceService } from '../service.service';
import {AfterViewInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Router} from '@angular/router';
import {MatTableDataSource} from '@angular/material/table';
import { MatChipsModule } from '@angular/material/chips';
import { DialogComponent } from '../dialog/dialog.component';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.scss']
})
export class StudentListComponent implements OnInit {
  student: Array<Student> = [];
  displayedColumns: string[] = ['id', 'fullName', 'email', 'entryType','streamType','gpa','action'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  constructor(private _route: Router, private _service: ServiceService,
    private dialog:MatDialog){}

  ngOnInit(): void {
    this.getStudents();
  }

  editStudent(row:any){
    console.log("Editing the data");
    this.dialog.open(DialogComponent,{data:row});
    //console.log(this.editData);
  }
  logout(){
    this._route.navigate(['/login']);
  }
  getStudents() {
    this._service.fetchStudentList().subscribe(
     // data => this.student = data,
      // error => console.log("Exception occurred 1"),
      {
        next:(res)=>{
          this.dataSource=new MatTableDataSource(res)
          this.dataSource.paginator=this.paginator
          this.dataSource.sort=this.sort
        },error:(err)=> { 
          alert("Exception occurred 1")}
      }
    )
  }
  deleteStudent(id:number) {
    this._service.deleteStudentList(id).subscribe(
      {
        next:(res)=>{
          alert("Student deleted sucessfully");
          this.getStudents();
        },error:(err)=> { 
          alert("Exception occurred 1")}
      }
    )
  }
  isEmpty()
  {
    if (this.student == null)
    {
      return true;
    }
    else { return false; }
  }
  openDialog() {
    const dialogRef = this.dialog.open(DialogComponent);

    }
}
