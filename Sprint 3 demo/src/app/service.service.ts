import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import { Student } from './student';
import { AnyCatcher } from 'rxjs/internal/AnyCatcher';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  constructor(private _http:HttpClient) { }

  fetchStudentList():Observable<any>{
    return this._http.get<any>('http://localhost:8081/students');
  }
  addStudentList(s:Student):Observable<any>{
    return this._http.post<any>('http://localhost:8081/students',s);
  }
  updateStudentList(s:any,id:number):Observable<any>{
    return this._http.put<any>('http://localhost:8081/student/'+id,s);
  }
  deleteStudentList(id:number):Observable<any>{
    return this._http.delete<any>('http://localhost:8081/students/'+id);
  }

  loginP(s:any):Observable<any>{
    return this._http.post<any>('http://localhost:8081/signin',s);
  }

  signupP(s:any):Observable<any>{
    return this._http.post<any>('http://localhost:8081/signup',s);
  }
}
