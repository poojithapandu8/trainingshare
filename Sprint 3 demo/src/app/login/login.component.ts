import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import {Router} from '@angular/router';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
 user=new User();
  constructor(private _service:ServiceService,private route:Router) { }
  ngOnInit(): void {
  }
  userlogin(){
  this._service.loginP(this.user).subscribe(data => {
    //alert("Login Sucesfull"+data);
  this.route.navigate([''])},
    error=>alert("Error : login failed"));
  }
}
