export class Student {
    id!:number;
    studentName!:string;
    email!:string;
    entryType!:boolean;
    courseType!:string;
    gradePoints!:number;
}
