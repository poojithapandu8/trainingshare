import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import {Router} from '@angular/router';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  user=new User();
  constructor(private _service: ServiceService,private route:Router) { }

  ngOnInit(): void {
  }
  usersignup(){
this._service.signupP(this.user).subscribe(data => {
  alert("Login Sucesfull"+data);},
  error=>alert("Error : Signup failed"));
  }

}
